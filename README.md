# war-addons

These are some addons for Warhammer Online. Most are simple, smallish patches for EA interface modules (or other addons). Some are WIP, I just dump whatever I have into this repo.

## Contents

| Name              | Description                                                                                                                                                                                                     |
| ----------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| BankWindowFix     | Fixes stacking of items when putting them in bank, fixes right-click not putting items into backpack.                                                                                                           |
| BiggerMacroWindow | (EA macro window with edited .xml) Show all macros in the macro window at once.                                                                                                                                 |
| BuffTrackerTweaks | Show more buffs on standard unitframes.                                                                                                                                                                         |
| Byakugan          | Automatic target marking on target/mouseover, yknow, to locate enemies.                                                                                                                                         |
| CastbarGCD        | Default castbar shows global cooldown when you cast an instant ability (like Effigy/Amethyst/Obsidian). Because instant abilities lack feedback in the big fights.                                              |
| CombatTextNames   | Adds ability names to the default EA combat text (damage numbers) and bold sans font.                                                                                                                           |
| FixStuckBuffs     | Fix stuck buffs on standard unitframes (happens when BuffHead is enabled).                                                                                                                                      |
| FollowTheLeader   | A macro to follow the warband leader. Addon handles updating the leader name in the macro.                                                                                                                      |
| HideHiddenFrames  | Hide hidden gray frames in UI editor. You can unhide them using a menu anyway.                                                                                                                                  |
| LargeBackpackFont | Large bold font in the backpack (currently except quest bag but who uses it).                                                                                                                                   |
| ShiftClickAbandon | Shift click on a quest in the Journal to quickly abandon it.                                                                                                                                                    |
| ShowMeTheBubbles  | Chat bubbles for `/wb`, `/1`, `/2` etc. Makes it easier to finds WB mates asking for help (also helps if you forget to read the chat sometimes).                                                                |
| TargetInfoRing    | (Modification of Talvinen's TargetRing) TargetRing with HP/name/carreer. Every other HUD addon (Pure, Effigy, RV) is bloated, so I've decided to add some bloat to targetring. The code could use some cleanup. |
| TintUnsellable    | Color unsellable items dark red when at vendor.                                                                                                                                                                 |

Other repos:

| Name                                                     | Description                                                                                                                                             |
| -------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [NerfedNB](https://gitlab.com/nwwdles/nerfednb/)         | stripped down NerfedButtons. No conditionals - it only checks if ability is castable at all, and if not, it will cast the next ability in the sequence. |
| [GatherButton](https://gitlab.com/nwwdles/gatherbutton/) | cultivate without clicking the mouse.                                                                                                                   |

## Roadmap

<details>

I would like to some day replace the big addons (RV, Pure, Effigy, MGR, WSCT, Enemy) with smaller ones that possibly have less overhead and much simpler code, while keeping the essential features.

- [x] Good enough cultivation workflow (MGR): [GatherButton](https://gitlab.com/nwwdles/gatherbutton/)
- [x] Castbar with GCD (Obsidian/Amethyst/Effigy): [CastbarGCD](castbargcd)
- [x] Readable combat text with icons/names (WSCT): [CombatTextNames](combattextnames)
- [x] Target HUD (Pure/Effigy/RV): [targetinforing](targetinforing)
- [ ] Target unitframes (Pure/Effigy/RV)
  - [ ] More compact than the default GUI
  - [ ] Sane buff tracker
  - [ ] Dispel indication (like Pure)
  - [ ] HP%
  - [ ] sane AP bar with AP regen delay indication (if possible)
- [ ] Warband unitframes (Enemy/Squared/RV)
  - [ ] Click casting
  - [ ] Effect indicators

### Misc ideas

- [ ] Targeting/following the guardee
- [ ] Nicer, more compact actionbars
- [ ] Standalone target assist compatible with Enemy messages
  - [ ] 'Auto'-assist (it's possible to set an action button to target an ally and it's possible to set an action button to send /assist command. since people are button-mashing anyway, this could be basically an autoassist)
- [ ] Performance modes (disable/reenable some non-essential addons' functionality and change user settings (e.g. player names rendering) etc. Either automatically, based on FPS, or with a hotkey. Basically, [this](https://www.returnofreckoning.com/forum/viewtopic.php?f=5&t=20075) but with a one-click configuration)
- [ ] Duplicate all on-screen announcements (forgot what it's called) in chat.
- [ ] Skill recommendation system instead of nerfedbuttons (reminder to meditate, cast runes/rituals/buffs/guards).
- [ ] ScenarioAlert demonstrates a cool way to communicate with the outer world, there should be some more insteresting uses.
  - It's possible to make an automated system of enemy location reports (somewhat, since you can't get precise coords of enemies) and an online map of players.
    - Doing this would be kinda bad for the gameplay because one could snitch on their own realm. If such a map were to implemented, only verified users shall be able to send location info, otherwise it'd be easy to spam the map with false info.
- [ ] Fix/reimplement bagomatic

</details>

## Licensing

¯\\\_(ツ)\_/¯

Most addons here are just patches to other people's code, I'm too lazy to research what it means to me in terms of licensing. A couple are actual forks, although licenses of the original works are unknown in some cases (EA code, targetring). If you're the copyright holder and you are angery 😠, you can contact me.

You can consider my contributions public domain or MIT/BSD/WTFPL/whatever-licensed and do whatever you want. I have a snippet with MIT license so I usually insert that one into files.
