BuffTrackerTweaks = {}

local buffcolumns = 5
local maxbuffs = 20
local verticalspacing = 2

function BuffTrackerTweaks.Initialize()
    BuffTracker.Create = BuffTrackerTweaks.Create -- yeah, a replacement
end

function BuffTrackerTweaks.Create( self, windowName, parentName, initialAnchor, buffTargetType, maxBuffCount, buffRowStride, showTimerLabels )
    buffRowStride = buffcolumns
    maxBuffCount = maxbuffs
    local newTracker =
    {
        m_buffData      = {},               -- Contains buff/effect data. Indexed by server effect id.
        m_buffMapping   = {},               -- Contains window id -> buff id mapping.
        m_targetType    = buffTargetType,   -- How this tracker knows which unit to query for buff/effects information...
        m_maxBuffs      = maxBuffCount,     -- How many windows this bufftracker creates...
        m_buffFrames    = {},
        m_buffRowStride = buffRowStride,    -- Number of buffs per row
    }

    local currentAnchor = initialAnchor

    for buffSlot = 1, maxBuffCount
    do
        local buffFrameName = windowName..buffSlot
        local buffFrame     = BuffFrame:Create( buffFrameName, parentName, buffSlot, buffTargetType, showTimerLabels )

        if ( buffFrame ~= nil )
        then
            newTracker.m_buffFrames[ buffSlot ] = buffFrame

            buffFrame:SetAnchor( currentAnchor )

            local nextSlot  = buffSlot + 1
            local remainder = math.fmod( nextSlot, buffRowStride )

            if ( remainder == 1 )
            then
                currentAnchor.Point             = "bottomleft"
                currentAnchor.RelativePoint     = "topleft"
                currentAnchor.RelativeTo        = windowName..( nextSlot - buffRowStride )
                currentAnchor.XOffset           = 0
                currentAnchor.YOffset           = verticalspacing -- vertical buff spacing between rows...parameterize?
            else
                currentAnchor.Point             = "right"
                currentAnchor.RelativePoint     = "left"
                currentAnchor.RelativeTo        = windowName..buffSlot
                currentAnchor.XOffset           = 0 -- horizontal buff spacing between columns...parameterize?
                currentAnchor.YOffset           = 0
            end
        end
    end

    newTracker = setmetatable( newTracker, self )
    newTracker.__index = self

    -- Load all buffs now.
    newTracker:Refresh()

    return newTracker
end