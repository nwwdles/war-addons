followTheLeader = {}

local leaderName = L""
local macroId
local macroName = L"Follow the Leader"
local macroIcon = 44

function followTheLeader.Initialize()
	macroId = followTheLeader.CreateMacro(macroName, L"", macroIcon)

	-- REGISTER EVENT HANDLERS (we probably don't need this many but i'm too lazy to test which ones we do need)
	RegisterEventHandler( SystemData.Events.GROUP_SET_LEADER, 					"followTheLeader.Update")
	RegisterEventHandler( SystemData.Events.GROUP_PLAYER_ADDED, 				"followTheLeader.Update")
	RegisterEventHandler( SystemData.Events.PLAYER_GROUP_LEADER_STATUS_UPDATED, "followTheLeader.Update")
	RegisterEventHandler( SystemData.Events.GROUP_ACCEPT_INVITATION, 			"followTheLeader.Update")
	RegisterEventHandler( SystemData.Events.BATTLEGROUP_ACCEPT_INVITATION, 		"followTheLeader.Update")
	RegisterEventHandler( SystemData.Events.GROUP_SETTINGS_UPDATED, 			"followTheLeader.Update")
	--RegisterEventHandler( SystemData.Events.GROUP_UPDATED, 					"followTheLeader.Update")
    --RegisterEventHandler( SystemData.Events.GROUP_STATUS_UPDATED, 			"followTheLeader.Update")
	--RegisterEventHandler( SystemData.Events.BATTLEGROUP_UPDATED, 				"followTheLeader.Update")
	--RegisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, 				"followTheLeader.OnChatText")

	followTheLeader.Print(L"followTheLeader initialized.")
end

function followTheLeader.Update()
	-- decide if we need to show or hide the button, update leader name so that button targets the actual leader
	if (IsWarBandActive() and GameData.Player.isInScenario == false) then
		local lead = followTheLeader.FixName(PartyUtils.GetWarbandLeader().name)
		if lead ~= leaderName then
			followTheLeader.OnLeaderNameChange(lead)
		end
	else
		-- followTheLeader.Print(L"No warband.")
		followTheLeader.SetMacro(macroId, macroName, L"", macroIcon)
	end
end

function followTheLeader.OnLeaderNameChange(newName)
	-- set button to target leader on user click (can't just target it through script)
	leaderName = newName
	followTheLeader.Print(L"Warband leader: "..newName)

	if newName == followTheLeader.FixName(GameData.Player.name) then
		-- we are the leader
		followTheLeader.SetMacro(macroId, macroName, L"", macroIcon)
	else
		followTheLeader.SetMacro(macroId, macroName, L"/follow "..newName, macroIcon)
		-- WindowSetGameActionData ("followTheLeaderWindow", GameData.PlayerActions.SET_TARGET, 0, newName or L"")
	end
end

function followTheLeader.Print(text)
	TextLogAddEntry("Chat", SystemData.ChatLogFilters.BATTLEGROUP, L"<icon00044> "..text)
end

function followTheLeader.FixName (str)
	-- is needed because target TargetInfo:UnitName(...) may not match PartyUtils.GetWarbandLeader().name even when they must be the same
	if (str == nil) then return nil end

	local str = str
	local pos = str:find (L"^", 1, true)
	if (pos) then str = str:sub (1, pos - 1) end

	return str
end

-- macro functions

function followTheLeader.GetMacroId(name)
	local macros = DataUtils.GetMacros()
	-- /script d(DataUtils.GetMacros())
	for slot = 1, EA_Window_Macro.NUM_MACROS do
		if macros[slot].name == name then
			return slot
		end
	end

	return nil
end

function followTheLeader.SetMacro(slot, name, text, iconId)
	SetMacroData(name, text, iconId, slot)
	EA_Window_Macro.UpdateDetails(slot)
end

function followTheLeader.CreateMacro(name, text, iconId)
	local slot = followTheLeader.GetMacroId(name)
	if (slot) then
		followTheLeader.SetMacro(slot, name, text, iconId) -- set icon and text
		return slot
	end

	local macros = DataUtils.GetMacros()
	for slot = 1, EA_Window_Macro.NUM_MACROS do
		if (macros[slot].text == L"") then
			followTheLeader.SetMacro(slot, name, text, iconId)
			return slot
		end
	end

	return nil
end