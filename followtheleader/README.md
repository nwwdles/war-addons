# Follow the Leader

A macro to follow wb leader. The addon handles updating the name in the macro 
when something changes (e.g. joining wb, changing the leader).