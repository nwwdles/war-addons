<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="FixStuckBuffs" version="1.0" date="2019-01-18">
        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0"/>

        <Author name="cupnoodles" />
        <Description text="fix stuck buffs"/>

        <Dependencies>
            <Dependency name="EATemplate_UnitFrames"/>
        </Dependencies>

        <Files>
            <File name="fixstuckbuffs.lua" />
        </Files>

        <OnInitialize>
            <CallFunction name="FixStuckBuffs.Initialize" />
        </OnInitialize>

        <WARInfo>
            <Categories>
                <!-- <Category name="CRAFTING" /> -->
            </Categories>
        </WARInfo>

    </UiMod>
</ModuleFile>
