FixStuckBuffs = {}

-- locals
local TargetUnitFrame_UpdateUnit_orig

function FixStuckBuffs.Initialize()
    -- prehook
    TargetUnitFrame_UpdateUnit_orig = TargetUnitFrame.UpdateUnit
    TargetUnitFrame.UpdateUnit = FixStuckBuffs.UpdateUnit
end

function FixStuckBuffs.UpdateUnit(self)

    -- refresh buffs on target change
    if (TargetInfo:UnitEntityId(self.m_UnitId) ~= self.old_entity_id) then
        self.old_entity_id = TargetInfo:UnitEntityId(self.m_UnitId)
        -- self.m_BuffTracker:ClearAllBuffs()
        self.m_BuffTracker:Refresh()
    end

    TargetUnitFrame_UpdateUnit_orig(self)
end
