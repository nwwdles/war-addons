<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="ShowMeTheBubbles" version="1.1" date="2019-12-04">

        <Author name="cupnoodles" email="cupn8dles@gmail.com" />
        <Description text="Additional speech bubbles if you want them" />

        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />

        <Dependencies>
        </Dependencies>

        <Files>
            <File name="showmethebubbles.lua" />
        </Files>

        <SavedVariables>
            <!-- <SavedVariable name="ShowMeTheBubbles.Settings" /> -->
        </SavedVariables>

        <OnInitialize>
            <CallFunction name="ShowMeTheBubbles.Initialize" />
        </OnInitialize>
        <OnUpdate/>
        <OnShutdown/>

        <WARInfo>
            <Categories>

            </Categories>
            <Careers>
                <Career name="BLACKGUARD" />
                <Career name="WITCH_ELF" />
                <Career name="DISCIPLE" />
                <Career name="SORCERER" />
                <Career name="IRON_BREAKER" />
                <Career name="SLAYER" />
                <Career name="RUNE_PRIEST" />
                <Career name="ENGINEER" />
                <Career name="BLACK_ORC" />
                <Career name="CHOPPA" />
                <Career name="SHAMAN" />
                <Career name="SQUIG_HERDER" />
                <Career name="WITCH_HUNTER" />
                <Career name="KNIGHT" />
                <Career name="BRIGHT_WIZARD" />
                <Career name="WARRIOR_PRIEST" />
                <Career name="CHOSEN" />
                <Career name="MARAUDER" />
                <Career name="ZEALOT" />
                <Career name="MAGUS" />
                <Career name="SWORDMASTER" />
                <Career name="SHADOW_WARRIOR" />
                <Career name="WHITE_LION" />
                <Career name="ARCHMAGE" />
            </Careers>
        </WARInfo>


    </UiMod>
</ModuleFile>
