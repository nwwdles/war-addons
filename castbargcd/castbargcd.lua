CastbarGcd = {}

local desiredtime
local ShowCastBar_orig
local HideCastBar_orig

function CastbarGcd.Initialize()
    ShowCastBar_orig = LayerTimerWindow.ShowCastBar
    LayerTimerWindow.ShowCastBar = CastbarGcd.ShowCastBar

    HideCastBar_orig = LayerTimerWindow.HideCastBar
    LayerTimerWindow.HideCastBar = CastbarGcd.HideCastBar
end

function CastbarGcd.HideCastBar(isCancel, fromQueuedCall)
    if desiredtime ~= 1.1 then
        HideCastBar_orig()
    else
        WindowStartAlphaAnimation ("LayerTimerWindowCastTimer", Window.AnimationType.EASE_OUT, 1, 0, 1.4, false, 0, 0)
    end
end

function CastbarGcd.ShowCastBar (abilityId, isChannel, desiredCastTime, holdCastBar)
    
    if desiredCastTime == 0 then
        desiredCastTime = 1.1
        isChannel = true
    end
    desiredtime = desiredCastTime
    ShowCastBar_orig(abilityId, isChannel, desiredCastTime, holdCastBar)
end