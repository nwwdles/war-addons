<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="CastbarGcd" version="1.0" date="1/1/2018">
        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0"/>

        <Author name="cupnoodles" />
        <Description text="a"/>

        <Dependencies>
            <Dependency name="EA_CastTimerWindow"/>
        </Dependencies>

        <Files>
            <File name="castbargcd.lua" />
        </Files>
        
        <SavedVariables>
            <!-- <SavedVariable name="Addon.Settings" /> -->
        </SavedVariables>

        <OnInitialize>
            <!-- <CreateWindow name="Addon" show="true" /> -->
            <CallFunction name="CastbarGcd.Initialize" />
        </OnInitialize>

        <OnUpdate>
        </OnUpdate>

        <OnShutdown/>
        
        <WARInfo>
            <Categories>
                <!-- <Category name="CRAFTING" /> -->
            </Categories>
        </WARInfo>

    </UiMod>
</ModuleFile>
