ByakuganMarker = Frame:Subclass("ByakuganWindow")

function ByakuganMarker:Create(windowName, parentWindow)
    local frame = self:CreateFromTemplate(windowName, parentWindow)

    if (frame) then
        frame.currentObjNum = 0
        frame:Show(false)
        frame:SetScale(Byakugan.Settings.MarkerScale)
    end

    return frame
end

function ByakuganMarker:UpdateDisplay(target)
    if Byakugan.Settings.SimpleMarker then
        local careerIcon
        if target.career > 0 then
            careerIcon = Icons.GetCareerIconIDFromCareerLine(target.career)
        else
            careerIcon = Byakugan.Settings.FallbackIcon
        end

        local text = L" <icon" .. towstring(careerIcon) .. L">"
        LabelSetText(self:GetName() .. "Name", text)

        return
    end

    local careerline = tostring(target.level) -- title will contain level and NPC title, e.g. 40(40) Lord Butcher
    local nameline = target.name -- career will contain Name and career icon
    local careercolor = {r = 255, g = 255, b = 255}

    if target.battleLevel ~= target.level then
        careerline = careerline .. "(" .. tostring(target.battleLevel) .. ")"
    end
    careerline = towstring(careerline)

    if target.tier > 0 then
        local tiername
        if target.tier == 3 then
            tiername = L"L"
        elseif target.tier == 2 then
            tiername = L"H"
        elseif target.tier == 1 then
            tiername = L"C"
        end
        careerline = careerline .. tiername
    end

    if target.career > 0 then
        local careerIcon = Icons.GetCareerIconIDFromCareerLine(target.career)
        nameline = nameline .. L" <icon" .. towstring(careerIcon) .. L">"
        careerline = target.careerName .. L" " .. careerline
        local ccolor = Byakugan.Settings.CareerColors[careerIcon]
        careercolor = ccolor
    else
        careerline = towstring(target.npcTitle) .. L" " .. careerline
    end

    local windowname = self:GetName()
    LabelSetText(windowname .. "Name", nameline)
    LabelSetTextColor(windowname .. "Name", target.relationshipColor.r,
                      target.relationshipColor.g, target.relationshipColor.b)

    LabelSetText(windowname .. "Career", careerline)
    LabelSetTextColor(windowname .. "Career", careercolor.r, careercolor.g,
                      careercolor.b)
end

function ByakuganMarker:HideIfOld()
    if self.currentObjNum <= 0 then
        return
    end

    local w = self:GetName()

    DetachWindowFromWorldObject(w, self.currentObjNum)
    WindowClearAnchors(w)
    local x_old, y_old = WindowGetScreenPosition(w)
    local dif = 100
    WindowAddAnchor(w, "center", "Root", "center", x_old + dif, y_old + dif)

    MoveWindowToWorldObject(w, self.currentObjNum, 1)
    local x, y = WindowGetScreenPosition(w)
    if x ~= x_old + dif or y ~= y_old + dif then
        WindowClearAnchors(w)
        AttachWindowToWorldObject(w, self.currentObjNum)
        MoveWindowToWorldObject(w, self.currentObjNum, 1)
    else
        self:Show(false)
    end
end

function ByakuganMarker:SetTarget(objNum, target)
    local wname = self:GetName()

    if ((objNum > 0) and (objNum ~= GameData.Player.worldObjNum) and
        (objNum ~= self.currentObjNum)) then
        if (self.currentObjNum) then
            DetachWindowFromWorldObject(wname, self.currentObjNum)
        end

        self.currentObjNum = objNum
        WindowSetShowing(wname, true)
        self:UpdateDisplay(target)
        AttachWindowToWorldObject(wname, objNum)
        MoveWindowToWorldObject(wname, objNum, 1)
        self:Show(true)
    elseif (self.currentObjNum and not objNum) then
        DetachWindowFromWorldObject(wname, self.currentObjNum)
        WindowSetShowing(wname, false)
        self:Show(false)
        self.currentObjNum = objNum
    end
end
