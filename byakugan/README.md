# Byakugan

Automatic target marking on mouseover.

Configuration is through `config.lua`.

Store next 3 targets in the priority group (6 max, although you can change the code yourself):

```
/script Byakugan.EnablePriorityGroupFor(3)
```

Disable adding markers to a group (to reenable, set to true):

```
/script Byakugan.Hostile.Active = false
/script Byakugan.Friendly.Active = false
```
