-- The MIT License (MIT)
--
-- Copyright (c) 2019 cupnoodles
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--
--
Byakugan = {}

Byakugan.PriorityNextN = -1

local update_interval = 0.1
local t = 0

function Byakugan.Initialize()
    if not Byakugan.Settings or not Byakugan.Settings.PERSISTENT_SETTINGS then
        Byakugan.Settings = Byakugan.DefaultSettings
    end

    Byakugan.Hostile = ByakuganMarkerGroup:Create("h", Byakugan.Settings
                                                      .HostileMarkers)
    Byakugan.Friendly = ByakuganMarkerGroup:Create("f", Byakugan.Settings
                                                       .FriendlyMarkers)
    Byakugan.Priority = ByakuganMarkerGroup:Create("p", 6)
    RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED,
                         "Byakugan.OnTargetsUpdate")
end

function Byakugan.OnUpdate(delta)
    t = t + delta
    if t < update_interval then
        return
    end

    t = t - update_interval
    Byakugan.Friendly:HideOldMarker()
    Byakugan.Hostile:HideOldMarker()
end

function Byakugan.EnablePriorityGroupFor(n)
    Byakugan.Priority.Active = (n > 1)
    Byakugan.PriorityNextN = n
end

function Byakugan.OnTargetsUpdate(targetClassification, targetId, targetType)
    TargetInfo:UpdateFromClient()

    local target = TargetInfo.m_Units[targetClassification]
    if Byakugan.Settings.Marks.IgnoredCareers[target.career] or
        Byakugan.Settings.Marks.IgnoredTypes[target.type] or
        Byakugan.Settings.Marks.IgnoredClassifications[targetClassification] then
        return
    end

    if Byakugan.Priority.Active then
        Byakugan.PriorityNextN = Byakugan.PriorityNextN - 1
        if Byakugan.PriorityNextN == 0 then
            Byakugan.Priority.Active = false
        end

        Byakugan.Priority:Attach(targetId, target)
        return
    end

    if TargetInfo:UnitIsFriendly(targetClassification) then
        Byakugan.Friendly:Attach(targetId, target)
    else
        Byakugan.Hostile:Attach(targetId, target)
    end
end
