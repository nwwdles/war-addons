ByakuganMarkerGroup = {
    Markers = {},
    WindowNamePart = "",
    Current = 0,

    Active = true,
    CurrentToClear = 0,
    MarkersIndexByObj = {}
}

function ByakuganMarkerGroup:Create(windowNamePart, numMarkers)
    self.WindowNamePart = windowNamePart
    self:SetNumMarkers(numMarkers)

    return self
end

function ByakuganMarkerGroup:HideAll()
    for _, v in ipairs(self.Markers) do
        if v:IsShowing() then
            DetachWindowFromWorldObject(v:GetName(), v.currentObjNum)
            self.MarkersIndexByObj[v.currentObjNum] = nil
            v:Show(false)
        end
    end
end

function ByakuganMarkerGroup:HideOldMarker()
    self.CurrentToClear = ((self.CurrentToClear) % #self.Markers) + 1
    local m = self.Markers[self.CurrentToClear]
    if not m or not m:IsShowing() then
        return
    end

    m:HideIfOld()
end

function ByakuganMarkerGroup:Attach(targetId, target)
    if not self.Active or self.MarkersIndexByObj[targetId] ~= nil then
        -- TODO: would be nice to move re-hovered targets on top of the stack
        return
    end

    self.Current = ((self.Current) % #self.Markers) + 1

    if self.Markers[self.Current] then
        local oldObject = self.Markers[self.Current].currentObjNum
        if oldObject then
            self.MarkersIndexByObj[oldObject] = nil
        end
    end

    self.MarkersIndexByObj[targetId] = self.Current

    local m = self.Markers[self.Current]
    m:SetTarget(targetId, target)
end

function ByakuganMarkerGroup:NewMarker()
    local w = "ByakuganMarker" .. self.WindowNamePart ..
                  tostring(#self.Markers + 1)
    self.Markers[#self.Markers + 1] = ByakuganMarker:Create(w)
end

function ByakuganMarkerGroup:PopMarker()
    self.Markers[#self.Markers]:Destroy()
    self.Markers[#self.Markers] = nil
end

function ByakuganMarkerGroup:SetNumMarkers(count)
    local d = count - #self.Markers
    if d > 0 then
        for _ = 1, d do
            self:NewMarker()
        end
    elseif d < 0 then
        for _ = 1, -d do
            self:PopMarker()
        end
    end
end
