Byakugan.DefaultSettings = {
    SimpleMarker = false,
    FriendlyMarkers = 24,
    HostileMarkers = 24,
    FallbackIcon = 51,
    MarkerScale = 0.6,
    Marks = {
        IgnoredClassifications = {
            ["mouseovertarget"] = false,
            ["selfhostiletarget"] = false,
            ["selffriendlytarget"] = false
        },
        IgnoredTypes = {
            [SystemData.TargetObjectType.SELF] = true,
            [SystemData.TargetObjectType.NONE] = true,
            [SystemData.TargetObjectType.ALLY_NON_PLAYER] = false,
            [SystemData.TargetObjectType.ALLY_PLAYER] = false,
            [SystemData.TargetObjectType.STATIC] = false,
            [SystemData.TargetObjectType.STATIC_ATTACKABLE] = false,
            [SystemData.TargetObjectType.ENEMY_NON_PLAYER] = false,
            [SystemData.TargetObjectType.ENEMY_PLAYER] = false
        },
        IgnoredCareers = {
            [0] = false, -- no career
            [GameData.CareerLine.ARCHMAGE] = false,
            [GameData.CareerLine.BLACK_ORC] = false,
            [GameData.CareerLine.BLACKGUARD] = false,
            [GameData.CareerLine.BRIGHT_WIZARD] = false,
            [GameData.CareerLine.CHOPPA] = false,
            [GameData.CareerLine.CHOSEN] = false,
            [GameData.CareerLine.DISCIPLE] = false,
            [GameData.CareerLine.ENGINEER] = false,
            [GameData.CareerLine.IRON_BREAKER] = false,
            [GameData.CareerLine.KNIGHT] = false,
            [GameData.CareerLine.MAGUS] = false,
            [GameData.CareerLine.MARAUDER] = false,
            [GameData.CareerLine.RUNE_PRIEST] = false,
            [GameData.CareerLine.SHADOW_WARRIOR] = false,
            [GameData.CareerLine.SHAMAN] = false,
            [GameData.CareerLine.SLAYER] = false,
            [GameData.CareerLine.SORCERER] = false,
            [GameData.CareerLine.SQUIG_HERDER] = false,
            [GameData.CareerLine.SWORDMASTER] = false,
            [GameData.CareerLine.WARRIOR_PRIEST] = false,
            [GameData.CareerLine.WHITE_LION] = false,
            [GameData.CareerLine.WITCH_ELF] = false,
            [GameData.CareerLine.WITCH_HUNTER] = false,
            [GameData.CareerLine.ZEALOT] = false
        }
    },

    -- TODO: check if those magic numbers are the same as GameData.CareerLine.* vars
    CareerColors = {
        [20180] = {r = 175, g = 206, b = 234}, -- | Archmage
        [20181] = {r = 126, g = 57, b = 96}, -- | Blackguard
        [20182] = {r = 51, g = 115, b = 21}, -- | Black Orc
        [20183] = {r = 255, g = 37, b = 10}, -- | Bright Wizard
        [20184] = {r = 147, g = 200, b = 120}, -- | Choppa
        [20185] = {r = 99, g = 109, b = 112}, -- | Chosen
        [20186] = {r = 204, g = 43, b = 211}, -- | Disciple of Khaine
        [20187] = {r = 232, g = 181, b = 128}, -- | Engineer
        [20188] = {r = 179, g = 1, b = 1}, -- | Slayer
        [20189] = {r = 155, g = 82, b = 65}, -- | Ironbreaker
        [20190] = {r = 246, g = 106, b = 19}, -- | Knight of the Blazing Sun
        [20191] = {r = 44, g = 81, b = 136}, -- | Magus
        [20192] = {r = 177, g = 136, b = 134}, -- | Marauder
        [20193] = {r = 246, g = 227, b = 148}, -- | Rune Priest
        [20194] = {r = 144, g = 142, b = 165}, -- | Shadow Warrior
        [20195] = {r = 147, g = 200, b = 120}, -- | Shaman
        [20196] = {r = 92, g = 24, b = 125}, -- | Sorcerer/Sorceress
        [20197] = {r = 60, g = 96, b = 48}, -- | Squig Herder
        [20198] = {r = 52, g = 96, b = 159}, -- | Swordmaster
        [20199] = {r = 237, g = 129, b = 100}, -- | Warrior Priest
        [20200] = {r = 227, g = 240, b = 246}, -- | White Lion
        [20201] = {r = 188, g = 106, b = 204}, -- | Witch Elf
        [20202] = {r = 150, g = 26, b = 18}, -- | Witch Hunter
        [20203] = {r = 144, g = 142, b = 145} -- | Zealot
    }
}
