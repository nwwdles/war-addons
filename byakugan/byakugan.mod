<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Byakugan" version="0.0.2" date="2020-07-18">
        <VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0"/>

        <Author name="cupnoodles" email="cupn8dles@gmail.com" />
        <Description text="The Byakugan is the kekkei genkai that originated from the Ōtsutsuki Clan, and later inherited by their distant descendants, the Hyūga Clan. It is regarded as one of the Three Great Dōjutsu, the others being the Sharingan and the Rinnegan."/>

        <Dependencies>
            <Dependency name="EASystem_WindowUtils"/>
        </Dependencies>

        <Files>
            <File name="byakugan.lua" />
            <File name="config.lua" />
            <File name="marker.lua" />
            <File name="markergroup.lua" />
            <File name="templates_byakugan.xml" />
        </Files>

        <SavedVariables>
            <SavedVariable name="Byakugan.Settings" />
        </SavedVariables>

        <OnInitialize>
            <CallFunction name="Byakugan.Initialize" />
        </OnInitialize>

        <OnUpdate>
            <CallFunction name="Byakugan.OnUpdate" />
        </OnUpdate>

        <OnShutdown/>

        <WARInfo>
            <Categories>
                <Category name="COMBAT" />
            </Categories>
        </WARInfo>

    </UiMod>
</ModuleFile>
