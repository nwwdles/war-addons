
LargeBackpackFont = {}

-- locals
local atStore = false
local EA_Window_Backpack_SetActionButton
local EA_Window_Backpack_OnShown

function LargeBackpackFont.Initialize()

    -- CreateWindowFromTemplate(SLOTS_NAME, "BackpackWindowEquipmentButtonGroupMine", WINDOW_NAME)

    LargeBackpackFont.RecreatePockets("EA_Window_BackpackPocketMine")
    EA_Window_Backpack.CreateMinimizedPocketWindows()
    EA_Window_Backpack.CreateAllQuestSlotWindows()
    
    EA_Window_Backpack.RedrawAllPockets()
    EA_Window_Backpack.ReAnchorPockets()
    
    -- TODO: This shouldn't have to refresh all slots
    EA_Window_Backpack.UpdateBackpackSlots()
end


function LargeBackpackFont.RecreatePockets(template)
    -- create all inventory pockets
	for __, pocketNumber in ipairs(EA_Window_Backpack.pockets.displayOrder) do
	
        local anchorWindow = EA_Window_Backpack.POCKETS_ANCHOR
        local pocketName, pocketIcon
        
		pocketName = EA_Window_Backpack.GetPocketName( pocketNumber )
        
        local pocket, backpackType = EA_Window_Backpack.GetInternalPocketAndType( pocketNumber )
    	local parentWindow = EA_Window_Backpack.views[backpackType].viewWindow        
        
        DestroyWindow(pocketName)
        if not DoesWindowExist( pocketName ) then
            
			CreateWindowFromTemplate( pocketName, template, parentWindow )
			
			-- Set up the timers
			ActionButtonGroupSetTimeFormat( pocketName.."Buttons", Window.TimeFormat.LARGEST_UNIT_TRUNCATE )
			ActionButtonGroupSetTimeAbbreviations( pocketName.."Buttons",
			                                      GetString(StringTables.Default.LABEL_TIMER_DAYS_ABBREVIATION),
			                                      GetString(StringTables.Default.LABEL_TIMER_HOURS_ABBREVIATION),
			                                      GetString(StringTables.Default.LABEL_TIMER_MINUTES_ABBREVIATION),
			                                      GetString(StringTables.Default.LABEL_TIMER_SECONDS_ABBREVIATION) )

            ButtonSetText( pocketName.."LockButtonBuyButton", GetString( StringTables.Default.LABEL_BUY ) )

		end
        
        -- Hiding filterbuttons on all backpacks except for purchased pockets in the the main inventory
        if( backpackType ~= EA_Window_Backpack.TYPE_INVENTORY or EA_Window_Backpack.GetPocketIsUnpurchased(pocketNumber) )
        then
            WindowSetShowing( pocketName.."FiltersButton", false )
        end

        if( not EA_Window_Backpack.GetPocketIsUnpurchased(pocketNumber) )
        then
            WindowSetShowing( pocketName.."LockButton", false )
        end
        
		WindowSetId( pocketName, pocketNumber)      
		EA_Window_Backpack.SetPocketTitleFromFilter( pocketNumber )
		
		pocketIcon = EA_Window_Backpack.GetPocketNonMinimizedIconName( pocketNumber )
		DynamicImageSetTextureSlice( pocketIcon, "Bag"..(pocket).."-Open" )
		
		if EA_Window_Backpack.GetPocketIsClosed( pocketNumber ) then
			WindowSetShowing( pocketName, false )
		else
			WindowSetShowing( pocketName, true )
			WindowClearAnchors( pocketName )
			WindowAddAnchor( pocketName, "bottomleft", anchorWindow, "topleft", 0, EA_Window_Backpack.SPACING_BETWEEN_POCKETS )
			anchorWindow = pocketName
		end
		
	end
end